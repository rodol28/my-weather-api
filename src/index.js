const express = require('express');
const app = express();
const path = require('path');
const router = require('./routes/routes-index');
const morgan = require('morgan');

// settings
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './views'));

// middleware
app.use(morgan('dev'));

// routes
app.use(router);


// static files
app.use(express.static(path.join(__dirname, './public')));





app.use((req, res) => {
    res.status(404).end('404 not found');
});

app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`)
});