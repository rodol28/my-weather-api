const router = require('express').Router();
const axios = require('axios');

router.get('/', (req, res) => {
    res.render('view-index');
});

router.get('/latitude/:latitude/longitude/:longitude', (req, res, next) => {
    if(!req.params.latitude || !req.params.longitude) {
        res.status(404).json({
            msg: 'error'
        });
    }

    let lat = parseFloat(req.params.latitude, 10);
    let lon = parseFloat(req.params.longitude, 10);
    console.log(lat, lon);

    axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=134204dd095c6b93ccc9b3b6a78be80d`)
    .then(response => {
        res.json({
            msg: 'success',
            temperature: response.data.main.temp,
            name:response.data.name
        })
    })
    .catch(error => {
        console.log(error);
        next()
        return;
    });

});

module.exports = router;

