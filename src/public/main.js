$(function () {
    var $h1 = $('h1');
    var $latitude = $('input[name=latitude]');
    var $longitude = $('input[name=longitude]');
    var $btnLocation = $('input[name=btnLocation]');

    $btnLocation.on('click', obtainPosition);

    function obtainPosition() { 
        const geoconfig = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 60000
        }

        navigator.geolocation.getCurrentPosition(show, errors, geoconfig);

    }

    function show(position) {
        $latitude.val(position.coords.latitude);
        $longitude.val(position.coords.longitude);
    }

    function errors(err) {
        alert(`Error: ${err.code} ${err.message}`);
    }

    $('form').on('submit', function (event) {
        event.preventDefault();
        var latitude = $.trim($latitude.val());
        var longitude = $.trim($longitude.val());

        $('h1').text('loading...');

        var req = $.ajax({
            url: `/latitude/${latitude}/longitude/${longitude}`,
            data: 'json'
        });

        
        req.done(function (data) {
            console.log(data);
            var temperatureF = parseFloat(data.temperature);
            var temperatureC = ((temperatureF / 6) - 32) * (5/9);

            $h1.html(`The temperature in ${data.name} is ${temperatureC.toFixed(2)}&#176 centigrados; in latitude ${latitude} and longitude ${longitude}`);
        });
        
        req.fail(function () {
            $h1.html('Error!');
        });
    });

});